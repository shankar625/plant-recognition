package com.shankar.leaf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.imgproc.Imgproc;
import org.opencv.video.BackgroundSubtractorMOG2;
import org.opencv.video.Video;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;

import com.shankar.leaf.util.CommonColors;
import com.shankar.leaf.util.ImageDisplayFromMatrix;
import com.shankar.leaf.util.ImageFetcher;

public class MainRunner {
	private static ImageFetcher imgFetcher;
	private static ImageDisplayFromMatrix imageDisplayer;
	private static int MAX_VALUE=255;
	private static int MIN_VALUE=185;
	
	public static void main(String[] args) throws IOException{
		//Load library
		System.loadLibrary( Core.NATIVE_LIBRARY_NAME ); 
		
		//Initialisation
		imgFetcher= new ImageFetcher();
		imageDisplayer = new ImageDisplayFromMatrix();
		List<MatOfPoint> contourList = new ArrayList<MatOfPoint>();
		//thresholdMat = new Mat();
		Mat grayImg=new Mat(),thresholdMat=new Mat();
		
		
		//get image and store into matrix
		Mat originalImage=imgFetcher.getMatrixFromImage();
		imageDisplayer.displayImage(originalImage, "orginal leaf ");
		
		//background sunbtraction
		BackgroundSubtractorMOG2 fgbg =Video.createBackgroundSubtractorMOG2();
		fgbg.apply(originalImage, new Mat());
		Imgcodecs.imwrite("C:/Users/shank/OneDrive/Desktop/D/leaf-gray-apply.jpg",originalImage);
		
		
		//convert RGB image to grayscale
		Imgproc.cvtColor(originalImage,grayImg,Imgproc.COLOR_RGB2GRAY);
		Imgcodecs.imwrite("C:/Users/shank/OneDrive/Desktop/D/leaf-gray.jpg",grayImg);
		
		/*
		 * for proper extraction of contours 
		 * image need to be converted to BINARY.
		 * So perform thresholding of image such 
		 * that background is black and part to 
		 * be focused is white(i.e Imgproc.THRESH_BINARY_INV)
		 * */
		Imgproc.threshold(grayImg, 
				thresholdMat, 
				MIN_VALUE,
				MAX_VALUE,
				Imgproc.THRESH_BINARY_INV);
		Imgcodecs.imwrite("C:/Users/shank/OneDrive/Desktop/D/leaf-threshold.jpg",thresholdMat);
		
		//find contours from thresholded image
		Mat hierarchy = new Mat();
		Imgproc.findContours(thresholdMat,
				contourList,
				hierarchy ,
				Imgproc.RETR_EXTERNAL,
				Imgproc.CHAIN_APPROX_SIMPLE);
		
		System.out.println("hierarchy \n"+hierarchy.dump());
		
		//plot contours data in image 
		Imgproc.drawContours(originalImage,contourList,-1, CommonColors.RED,2);
		
		Imgcodecs.imwrite("C:/Users/shank/OneDrive/Desktop/D/leaf-contour-detect.jpg",originalImage);
		//imageDisplayer.displayImage(originalImage, "leaf contour is plotted");
		
		//get array of points
		
		//displayContours(contourList);
		Point c =findCentre(contourList.get(1).toArray());
		Imgproc.circle(originalImage,c,2,CommonColors.RED,5);
		Imgcodecs.imwrite("C:/Users/shank/OneDrive/Desktop/D/leaf-center.jpg",originalImage);
		System.out.println("center found at x="+c.x+", y="+c.y);
	}
	
	private static void displayContours(List<MatOfPoint> contourList)
	{
		Point[] mPoints;
		for(int i=0;i<contourList.size();i++)
		{
			mPoints =contourList.get(i).toArray();
			System.out.println("lot num "+(i+1));
			for(int j=0;j<mPoints.length;j++)
			{
				System.out.println("x="+mPoints[j].x+", y="+mPoints[j].y);
			}
			
		}
	}
	private void saveImage(Mat image, String name)
	{
		Imgcodecs.imwrite(name, image); 
	}
	
	/*
	 * This function finds average centre from given set of points
	 * mPoints is array of points that bounds surface
	 * */
	private static Point findCentre(Point[] mPoints)
	{
		double x=0.0;
		double y=0.0;
		for(int i=0;i<mPoints.length;i++)
		{
			x+=mPoints[i].x;
			y+=mPoints[i].y;
		}
		return new Point(x/mPoints.length,y/mPoints.length);
	}

}
